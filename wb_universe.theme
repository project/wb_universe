<?php
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\wb_universe\ThemeSettings;
use Drupal\Core\Render\Element;
use Drupal\wb_universe\Handler\MailStyles;
use Drupal\wb_universe\Handler\Wb_universePreRender;

/**
 * Use this fonction on module to alert meta data
 * mymodule_page_attachments_alter(array &$page)
 *
 * @param array $variables
 */
function wb_universe_preprocess_html(&$variables) {
  $view_id = \Drupal::routeMatch()->getParameter('view_id');
  // si on est sur une page de view.
  if ($view_id) {
    $paramettres = [
      "page-view",
      $view_id,
      \Drupal::routeMatch()->getParameter('display_id')
    ];
  }
  else
    $paramettres = \Drupal::routeMatch()->getParameters()->keys();
  $_css = [];
  $uid = \Drupal::currentUser()->id();
  if ($uid && \Drupal\user\Entity\User::load($uid)->hasRole('administrator')) {
    $_css[] = 'administrator';
  }
  // Add.
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $_css[] = 'front-page';
  }
  else {
    $_css[] = 'not-front-page';
  }
  if (\Drupal::languageManager()->getCurrentLanguage()->getDirection() == \Drupal\Core\Language\LanguageInterface::DIRECTION_RTL)
    $_css[] = 'rtl';
  
  $variables['html_attributes']->setAttribute('class', $_css);
  if (!empty($variables['attributes']['class'])) {
    $variables['attributes']['class'] = [
      ...$variables['attributes']['class'],
      ...$_css,
      ...$paramettres
    ];
  }
  else
    $variables['attributes']['class'] = [
      ...$_css,
      ...$paramettres
    ];
  
  if ($IdAnalytique = theme_get_setting('google-analytics-gtag')) {
    $scriptUrl = [
      '#tag' => 'script',
      '#attributes' => [
        'async' => 'async',
        'src' => 'https://www.googletagmanager.com/gtag/js?id=' . $IdAnalytique
      ]
    ];
    $scriptInit = [
      '#tag' => 'script',
      '#value' => "window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '$IdAnalytique');
            "
    ];
    $variables['page']['#attached']['html_head'][] = [
      $scriptUrl,
      'google-analytics-gtag'
    ];
    $variables['page']['#attached']['html_head'][] = [
      $scriptInit,
      'google-analytics-gtag-value'
    ];
  }
  if ($facebookApiId = theme_get_setting('facebook-api-id')) {
    $languageId = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if (!$languageId)
      $languageId = 'fr';
    $scriptUrl = [
      '#tag' => 'script',
      '#attributes' => [
        'async' => 'async',
        'defer' => 'defer',
        'src' => 'https://connect.facebook.net/' . $languageId . '/sdk.js',
        'crossorigin' => 'anonymous'
      ]
    ];
    $scriptInit = [
      '#tag' => 'script',
      '#value' => " window.fbAsyncInit = function() {
                FB.init({
                  appId            : $facebookApiId,
                  xfbml            : true,
                  version          : 'v20.0'
                });
                FB.AppEvents.logPageView();   
                // Permet de detecter le chargement de facebook.
                var event = new CustomEvent('hbk_fbInit');
                document.dispatchEvent(event);
              };
            "
    ];
    $variables['page']['#attached']['html_head'][] = [
      $scriptUrl,
      'facebook-url'
    ];
    $variables['page']['#attached']['html_head'][] = [
      $scriptInit,
      'facebook-init'
    ];
  }
}

/**
 * Implement hook_preprocess_mimemail_message
 */
function wb_universe_preprocess_mimemail_message(array &$variables) {
  MailStyles::formatter($variables);
}

/**
 * Implement hook_preprocess_page
 */
function wb_universe_preprocess_page(&$variables) {
  $routeName = \Drupal::routeMatch()->getRouteName();
  /**
   * wb_universe_pages:
   * applique les classes et ou configuration provenant de la configuration du
   * theme.
   */
  $class = theme_get_setting('wb_universe_pages.terms.' . ThemeSettings::getValidKeyForConfig($routeName));
  if (!$class) {
    $class = theme_get_setting('wb_universe_pages.search.' . ThemeSettings::getValidKeyForConfig($routeName));
  }
  if (!$class) {
    $class = theme_get_setting('wb_universe_pages.views.' . ThemeSettings::getValidKeyForConfig($routeName));
  }
  //
  if ($class) {
    // Au depart on utilisait attributes_container, mais attributes est plus
    // adapté.
    $variables["attributes"]['class'][] = $class;
  }
  
  /**
   * Build layouts
   */
  $wb_universe_layout = theme_get_setting('wb_universe_layout');
  if (!empty($wb_universe_layout)) {
    $show_sidebar_left = $wb_universe_layout['sidebar_left']['show'] ?? false;
    $show_sidebar_right = $wb_universe_layout['sidebar_right']['show'] ?? false;
    $variables['show_sidebar_left'] = $show_sidebar_left;
    $variables['show_sidebar_right'] = $show_sidebar_right;
    if (!empty($variables['page']['sidebar_left']) || !empty($variables['page']['sidebar_right'])) {
      ThemeSettings::$hasSideBar = true;
    }
  }
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_commerce_product_variation(&$variables) {
  /**
   *
   * @var \Drupal\commerce_product\Entity\ProductVariation $product_variation_entity
   */
  $product_variation_entity = $variables['product_variation_entity'];
  $variables['attributes'] = [
    'class' => [
      'commerce_product_variation',
      'view_mode__' . $variables['elements']['#view_mode']
    ],
    'commerce_id' => $product_variation_entity->getProductId(),
    'commerce_variant_id' => $product_variation_entity->id()
  ];
}

/**
 * implement hook_preprocess_block
 *
 * @param array $vars
 */
function wb_universe_preprocess_block(&$vars) {
  if ($vars['plugin_id'] == 'local_tasks_block') {
    $custom_class = theme_get_setting('wb_universe_blocks.local_tasks_block.custom_class');
    if ($custom_class) {
      $vars['attributes']['class'] = $custom_class;
    }
  }
}

/**
 * implement hook_preprocess_table
 *
 * @param array $vars
 */
function wb_universe_preprocess_table(&$vars) {
  $vars["attributes"]['class'][] = theme_get_setting('wb_universe_tables.custom_class');
  $vars['hbk_active_container'] = theme_get_setting('wb_universe_tables.custom_class');
  $vars['custom_class_container'] = theme_get_setting('wb_universe_tables.custom_class_container');
}

function wb_universe_preprocess_views_view_table(&$vars) {
  $vars["attributes"]['class'][] = theme_get_setting('wb_universe_tables.custom_class');
  $vars['hbk_active_container'] = theme_get_setting('wb_universe_tables.custom_class');
  $vars['custom_class_container'] = theme_get_setting('wb_universe_tables.custom_class_container');
}

/**
 * Implement hook_preprocess_page
 */
function wb_universe_preprocess_page__user(&$variables) {
  $class = theme_get_setting('wb_universe_pages.user.user_all');
  if ($class) {
    $variables["attributes_container"] = $class;
  }
}

/**
 *
 * @param
 *        $variables
 */
function wb_universe_preprocess_fieldset(&$variables) {
  /**
   * Recuperer les classes passée aux niveaux hook_form_alter et les ajouter.
   */
  // Class pour le titre.
  if (!empty($variables['element']['#legend'])) {
    /**
     *
     * @var Drupal\Core\Template\Attribute $attribute
     */
    $attribute = $variables['legend']['attributes'];
    if (!empty($variables['element']['#legend']['attributes']))
      foreach ($variables['element']['#legend']['attributes'] as $k => $value) {
        $attribute->setAttribute($k, $value);
      }
    $variables['legend']['attributes'] = $attribute;
    //
    if (!empty($variables['children'])) {
      $variables['children'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            ""
          ]
        ],
        '#value' => $variables['children']
      ];
    }
  }
}

/**
 * implement hook_page_attachments_alter
 *
 * @param array $attachments
 */
function wb_universe_page_attachments_alter(array &$attachments) {
  $stripe_settings = theme_get_setting('stripe_settings');
  $general_settings = theme_get_setting('general_settings');
  if (!empty($general_settings['load_bootstrap'])) {
    $attachments["#attached"]["library"][] = "wb_universe/bootstrap_default";
  }
  if (!empty($stripe_settings['load_stripe'])) {
    $attachments["#attached"]["library"][] = "wb_universe/stripe_v3";
  }
}

/**
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function _wb_universe_form_system_theme_settings_submit(array &$form, FormStateInterface $form_state) {
  // Add custom style in custom file.
  /**
   *
   * @var \Drupal\layoutgenentitystyles\Services\LayoutgenentitystylesServices $styleTheme
   */
  $styleTheme = \Drupal::service("layoutgenentitystyles.add.style.theme");
  $template_class = theme_get_setting('wb_universe_blocks.local_tasks_block.template_class');
  $template_class = $form_state->getValue([
    'wb_universe_blocks',
    'local_tasks_block',
    'template_class'
  ]);
  if (!empty($template_class)) {
    $styleTheme->addStyleFromTheme($template_class, 'wb_universe_blocks', 'local_tasks_block');
  }
}

/**
 * Formatage des champs.
 *
 * @param array $variables
 */
function wb_universe_preprocess_field(&$variables) {
  /**
   * Class for type boolean.
   */
  if (isset($variables['element']['#field_type']) && $variables['element']['#field_type'] == 'boolean') {
    $value = $variables['element']['#object']->get($variables['field_name'])->getValue();
    if (!empty($value[0])) {
      $variables['attributes']['class'][] = 'boolean-' . $value[0]['value'];
    }
  }
  /**
   * for node link
   */
  elseif (isset($variables['element']['#field_name']) && $variables['element']['#field_name'] == 'node_link') {
    foreach ($variables['items'] as $key => $value) {
      $variables['items'][$key]['attributes']->addClass('btn');
      $variables['items'][$key]['attributes']->addClass('btn-outline-primary');
    }
  }
}

function wb_universe_preprocess_image(&$variables) {
  $variables['attributes']['class'][] = 'img-fluid';
}

/**
 * On surcharge les buttons qui ne le sont pas.
 *
 * @param array $variables
 */
function wb_universe_preprocess_input__submit(&$variables) {
  $class = ' btn ';
  if (!empty($variables['element']['#button_type']) && $variables['element']['#button_type'] == 'primary') {
    $class .= ' btn-primary ';
  }
  else {
    $class .= ' btn-outline-secondary ';
  }
  // if (\Drupal::currentUser()->id() == 1)
  // dump($variables['attributes']['class']);
  if (!empty($variables['attributes'])) {
    if (!empty($variables['attributes']['class'])) {
      // si btn exite deja, alors on suppose que le button est geré ailleurs.
      if (!in_array('btn', $variables['attributes']['class'])) {
        $variables['attributes']['class'][] = $class;
      }
    }
  }
  else {
    $variables['attributes']['class'] = $class;
  }
}

function wb_universe_preprocess_menu_local_action(&$variables) {
  /**
   * $variables['link']['#options']['Attributes'] must be array.
   */
  if (isset($variables['link']['#options']['attributes'])) {
    $variables['link']['#options']['attributes']['class'][] = 'nav-link';
  }
}

function wb_universe_preprocess_menu_local_task(&$variables) {
  /**
   * $variables['link']['#options']['Attributes'] must be array.
   */
  if (isset($variables['link']['#options']['attributes'])) {
    $variables['link']['#options']['attributes']['class'][] = 'nav-link';
  }
  else {
    $variables['link']['#options']['attributes'] = [
      'class' => [
        'nav-link'
      ]
    ];
  }
}

/**
 * Implements hook_preprocess_HOOK() menu__main
 */
function wb_universe_preprocess_menu(&$variables) {
  if (!empty($variables['menu_name'])) {
    $class_menu = theme_get_setting('wb_universe_menus.' . $variables['menu_name'] . '.class_menu');
    if (!empty($class_menu))
      $variables['attributes']['class'][] = $class_menu;
  }
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_pager(&$variables) {
  if (!empty($variables['items']['next'])) {
    $variables['items']['next']['attributes']->addClass('page-link');
  }
  if (!empty($variables['items']['first'])) {
    $variables['items']['first']['attributes']->addClass('page-link');
  }
  if (!empty($variables['items']['last'])) {
    $variables['items']['last']['attributes']->addClass('page-link');
  }
  if (!empty($variables['items']['previous'])) {
    $variables['items']['previous']['attributes']->addClass('page-link');
  }
  if (!empty($variables['items']['pages'])) {
    foreach ($variables['items']['pages'] as &$item) {
      $item['attributes']->addClass('page-link');
    }
  }
}

/**
 * Implement hook_form_alter
 *
 * @param array $variables
 */
function wb_universe_preprocess_input(&$variables) {
  if (in_array($variables['element']['#type'], ThemeSettings::$control_inputs)) {
    $variables['attributes']['class'][] = 'form-control';
  }
  elseif (in_array($variables['element']['#type'], ThemeSettings::$check_inputs)) {
    $variables['attributes']['class'][] = 'form-check-input';
  }
}

/**
 * Implement hook_preprocess_HOOK
 *
 * @param array $variables
 */
function wb_universe_preprocess_select(&$variables) {
  $variables['attributes']['class'][] = 'form-control w-auto';
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_input__tel(&$variables) {
  $variables['attributes']['class'][] = 'form-control';
}

function wb_universe_preprocess_image_widget(&$vars) {
  $vars['attributes']['class'][] = 'd-flex';
}

function wb_universe_preprocess_file_managed_file(&$vars) {
  $vars['attributes']['class'][] = 'd-flex';
}

/**
 * Implement hook_form_alter
 *
 * @param array $variables
 */
function wb_universe_preprocess_textarea(&$variables) {
  $variables['attributes']['class'][] = 'form-control';
}

function wb_universe_preprocess_form_element(&$variables) {
  if (!empty($variables["element"]["#type"])) {
    // if ($variables['element']['#type'] == 'checkbox') {
    // if (!empty($variables["label"]["#children"])) {
    // $variables["children"] = new
    // FormattableMarkup($variables["label"]["#children"], []);
    // $variables["label"]['#attributes']['class'][] = 'custom-control-label';
    // unset($variables["label"]["#children"]);
    // }
    // }
    
    // On ajoute la possiblité d'ajouter des class provenant de la
    // configuration du theme.
    $wb_universe_forms = theme_get_setting('wb_universe_forms');
    if (!empty($wb_universe_forms[$variables["element"]["#type"]]))
      $variables['attributes']['class'][] = $wb_universe_forms[$variables["element"]["#type"]];
    // On formatte directement les champs password utiliser dans
    // password_confirm.
    if (!empty($variables['name']) && ($variables['name'] == "pass[pass1]" || $variables['name'] == "pass[pass2]")) {
      $variables['attributes']['class'][] = 'col-md-6 px-1 pe-md-3';
    }
    // On ajoute les class sur les labels
    if (in_array($variables['element']['#type'], ThemeSettings::$control_inputs)) {
      $variables['label']['#attributes']['class'][] = 'form-label';
    }
    elseif (in_array($variables['element']['#type'], ThemeSettings::$check_inputs)) {
      $variables['label']['#attributes']['class'][] = 'form-check-label';
      $variables['attributes']['class'][] = 'form-check';
    }
    // le champs password_confirm est tres peu utiliser, on fait un formatage
    // endur.
    elseif ($variables['element']['#type'] == 'password_confirm') {
      $variables['attributes']['class'][] = 'mb-4 row g-0 py-4 px-3 px-md-4 bg-light border';
    }
    
    // formatage de la description des champs.
    if (!empty($variables["description"]["attributes"])) {
      if ($variables["description"]["attributes"] instanceof \Drupal\Core\Template\Attribute)
        $variables["description"]["attributes"]->addClass('small text-secondary fst-italic opacity-75');
    }
  }
  
  // Le formatteur du label ne contient pas de type, on le passe à ce niveau.
  // ( plus necessaire car on formatte le label à ce niveau ).
  // $variables['label']['#type_custom'] = $variables["element"]["#type"];
  // dump($variables);
  /**
   * On a des cas assez rar, ou on souhaite passé les classes à partir de
   * l'enfant.
   * Example : le formattage du champs adresse.
   * On va utiliser #class_wrappers
   */
  if (!empty($variables['element']['#class_wrappers'])) {
    $variables['attributes']['class'][] = $variables['element']['#class_wrappers'];
  }
}

/**
 *
 * @param array $form
 * @param FormStateInterface $form_state
 */
function wb_universe_form_views_exposed_form_alter(&$form, FormStateInterface $form_state) {
  $class = theme_get_setting('wb_universe_forms.views_exposed_form.class');
  if ($class) {
    $keys = Element::children($form);
    $ignorefields = [
      'form_build_id',
      'form_id',
      'form_token'
    ];
    foreach ($keys as $field_input) {
      if (!in_array($field_input, $ignorefields)) {
        if ('actions' == $field_input) {
          $form[$field_input]['submit']['#attributes']['class'][] = 'btn btn-primary';
          $form[$field_input]['submit']['#attributes']['class'][] = 'btn-lg';
        }
        else {
          // if ($form[$field_input]['#type'] !== 'radios') {
          // }
          $form[$field_input]['#attributes']['class'][] = $class;
          $form[$field_input]['#theme_wrappers'][] = 'form_element__clean';
        }
      }
    }
  }
}

/**
 * SUggestion pour les menus.
 *
 * @param array $suggestions
 * @param array $variables
 */
function wb_universe_theme_suggestions_menu_alter(array &$suggestions, array $variables) {
  if (!empty($variables['menu_name'])) {
    $suggestion = theme_get_setting('wb_universe_menus.' . $variables['menu_name'] . '.template');
    if ($suggestion) {
      $suggestions[] = 'menu__' . $suggestion;
    }
  }
}

/**
 * Suggestion pour les vues.
 * (besoin de base : On a besoin de pouvoir formatter de facon precise la vue du
 * panier).
 */
function wb_universe_theme_suggestions_views_view_table_alter(array &$suggestions, array $variables) {
  /**
   *
   * @var \Drupal\views\ViewExecutable $view
   */
  $view = $variables['view'];
  $suggestions[] = 'views_view_table__' . $view->id();
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_region(&$vars) {
  $class_region = theme_get_setting('wb_universe_regions.' . $vars["region"] . '.class_region');
  if (!empty($class_region)) {
    $vars['attributes']['class'][] = $class_region;
  }
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_region__sidebar_left(&$vars) {
  $wb_universe_layout = theme_get_setting('wb_universe_layout');
  $vars['attributes']['class'][] = $wb_universe_layout['sidebar_left']['class'];
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_region__sidebar_right(&$vars) {
  $wb_universe_layout = theme_get_setting('wb_universe_layout');
  $vars['attributes']['class'][] = $wb_universe_layout['sidebar_right']['class'];
}

/**
 *
 * @param array $variables
 */
function wb_universe_preprocess_region__content(&$vars) {
  _apply_style_region__content($vars);
}

/**
 * Cette region est definie par le theme bootstrap_barrio.
 *
 * @param array $vars
 */
function wb_universe_preprocess_region__nowrap(&$vars) {
  if ($vars['elements']['#region'] == 'content') {
    _apply_style_region__content($vars);
  }
}

function _apply_style_region__content(&$vars) {
  if (ThemeSettings::$hasSideBar) {
    $wb_universe_layout = theme_get_setting('wb_universe_layout');
    $vars['attributes']['class'][] = $wb_universe_layout['content']['class'];
  }
}
